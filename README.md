# README #

Este README documenta os passos necess�rios para iniciar a aplica��o estoque-ubs.

### What is this repository for? ###

* Quick summary
Aplica��o Spring Boot que manipula o controle de estoque.
* Version
1.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

Para compilar e iniciar a aplica��o � necess�rio criar os Debug Configuration no Eclipse. 

* Configuration

Ap�s feito o clone do projeto, � necess�rio copi�-lo para: c:\workspace\GFT\gft

Para compilar � necess�rio criar um Maven Configuration
com o Goals: spring-boot:run

Para debugar a aplica��o � necess�rio criar um Remote Java Application apontando para localhost na porta 9898.

Para acessar a aplica��o � necess�rio acessar a url: (http://localhost:8080/swagger-ui.html)

* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact