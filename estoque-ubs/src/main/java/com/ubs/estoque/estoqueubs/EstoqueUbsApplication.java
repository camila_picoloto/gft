package com.ubs.estoque.estoqueubs;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ubs.estoque.estoqueubs.model.Product;

@SpringBootApplication
public class EstoqueUbsApplication {

	public static Product estoque;
	
	public static void main(String[] args) {
		SpringApplication.run(EstoqueUbsApplication.class, args);
		
	}
	
	@EventListener(ApplicationReadyEvent.class)
	public void leJsonAfterStartup() {
	    System.out.println("started up");
	    
	    ObjectMapper mapper = new ObjectMapper();

		try {
			//JSON file to Java object
			//BufferedReader br1 = new BufferedReader(new FileReader("c:\\workspace\\GFT\\gft\\teste.json"));
			//estoque = mapper.readValue(br1, Product.class);
	        
			
            BufferedReader br1 = new BufferedReader(new FileReader("c:\\workspace\\GFT\\gft\\data_1.json"));
            estoque = mapper.readValue(br1, Product.class);
			
            BufferedReader br2 = new BufferedReader(new FileReader("c:\\workspace\\GFT\\gft\\data_2.json"));
            Product estoque2 = mapper.readValue(br2, Product.class);
			estoque.getData().addAll(estoque2.getData());
			
            BufferedReader br3 = new BufferedReader(new FileReader("c:\\workspace\\GFT\\gft\\data_3.json"));
            Product estoque3 = mapper.readValue(br3, Product.class);
            estoque.getData().addAll(estoque3.getData());
			
            BufferedReader br4 = new BufferedReader(new FileReader("c:\\workspace\\GFT\\gft\\data_4.json"));
            Product estoque4 = mapper.readValue(br4, Product.class);
            estoque.getData().addAll(estoque4.getData());
            
			
			System.out.println("finished");
			
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    
	}

}
