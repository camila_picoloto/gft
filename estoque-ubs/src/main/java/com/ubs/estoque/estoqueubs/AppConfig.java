package com.ubs.estoque.estoqueubs;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.ubs.estoque.estoqueubs.model.Data;
import com.ubs.estoque.estoqueubs.model.Product;

@Configuration
@ComponentScan({"com.ubs.estoque.estoqueubs", "com.ubs.estoque.estoqueubs.model"})
public class AppConfig {
	
	@Bean
    public Product setProduct() {
		Product p = new Product();
		return p;
    }

    @Bean
    @Qualifier("listData")
    public Set<Data> setData() {
        return new HashSet<Data>();
    }
}