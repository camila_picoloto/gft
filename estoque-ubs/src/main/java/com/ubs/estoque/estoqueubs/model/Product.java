package com.ubs.estoque.estoqueubs.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"data"
})

@Component("product")
public class Product {
	
	@JsonIgnore
	private String id;
	
	@JsonProperty("data")
	private Set<Data> data;
	
	public Product() {
		super();
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}



	@JsonProperty("data")
	public Set<Data> getData() {
		return data;
	}


	@Autowired
	@Qualifier("listData")
	@JsonProperty("data")
	public void setData(Set<Data> data) {
		this.data = data;
	}
	
	public Integer sumOfQuantity() {
		Integer q = data.stream()
				.map(x -> x.getQuantity())
				.reduce(0, Integer::sum);
		return q;
	}
	
	
	public BigDecimal sumOfVolume() {
		BigDecimal b = data.stream()
				.map(x -> x.getVolume())
				.reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2, RoundingMode.HALF_DOWN);
		return b;
	}
	
	public BigDecimal avgPrice() {
		BigDecimal v = sumOfVolume();
		BigDecimal q = new BigDecimal(sumOfQuantity()).setScale(2, RoundingMode.HALF_DOWN);
		BigDecimal t = v.divide(q, 6, RoundingMode.HALF_DOWN);
		
		return t;
	}
	
	
}
