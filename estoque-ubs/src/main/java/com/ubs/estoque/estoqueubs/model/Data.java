package com.ubs.estoque.estoqueubs.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ubs.estoque.estoqueubs.util.LocalCurrencyDeserializer;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"product",
"quantity",
"price",
"type",
"industry",
"origin"
})

@Component("data")
public class Data {

	@JsonProperty("product")
	private String product;
	
	@JsonProperty("quantity")
	private Integer quantity;
	
	@JsonProperty("price")
	@JsonDeserialize(using = LocalCurrencyDeserializer.class)
	private BigDecimal price;
	
	@JsonProperty("type")
	private String type;
	
	@JsonProperty("industry")
	private String industry;
	
	@JsonProperty("origin")
	private String origin;
	
	public Data() {
		super();
	}
	
	
	public Data(String product, Integer quantity, BigDecimal price, String type, String industry, String origin) {
		super();
		this.product = product;
		this.quantity = quantity;
		this.price = price;
		this.type = type;
		this.industry = industry;
		this.origin = origin;
	}



	public Data(String product, Integer quantity, BigDecimal price) {
		super();
		this.product = product;
		this.quantity = quantity;
		this.price = price;
	}


	@JsonProperty("product")
	public String getProduct() {
		return product;
	}

	@JsonProperty("product")
	public void setProduct(String product) {
		this.product = product;
	}

	@JsonProperty("quantity")
	public Integer getQuantity() {
		return quantity;
	}

	@JsonProperty("quantity")
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@JsonProperty("price")
	public BigDecimal getPrice() {
		return price;
	}

	@JsonProperty("price")
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("industry")
	public String getIndustry() {
		return industry;
	}

	@JsonProperty("industry")
	public void setIndustry(String industry) {
		this.industry = industry;
	}

	@JsonProperty("origin")
	public String getOrigin() {
		return origin;
	}

	@JsonProperty("origin")
	public void setOrigin(String origin) {
		this.origin = origin;
	}


	public BigDecimal getVolume() {
		return this.price.multiply(new BigDecimal(this.quantity)).setScale(2,  RoundingMode.HALF_DOWN);
	}

	public BigDecimal getAvgPrice() {
		BigDecimal t = this.getVolume().divide(new BigDecimal(this.getQuantity()), 4, RoundingMode.HALF_DOWN);
		
		return t;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((industry == null) ? 0 : industry.hashCode());
		result = prime * result + ((origin == null) ? 0 : origin.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Data other = (Data) obj;
		if (industry == null) {
			if (other.industry != null)
				return false;
		} else if (!industry.equals(other.industry))
			return false;
		if (origin == null) {
			if (other.origin != null)
				return false;
		} else if (!origin.equals(other.origin))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (product == null) {
			if (other.product != null)
				return false;
		} else if (!product.equals(other.product))
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
	
}
