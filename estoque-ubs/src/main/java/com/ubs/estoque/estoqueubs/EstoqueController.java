package com.ubs.estoque.estoqueubs;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ubs.estoque.estoqueubs.model.Data;
import com.ubs.estoque.estoqueubs.model.Product;

@RestController
public class EstoqueController {

	private Product estoqueProduto;
	
	@Autowired
    public void setProduct(Product product) {
		this.estoqueProduto = product;
    }
	
	
	@PostMapping("/vendeEstoque")
	public List<Product> vendeEstoque(@RequestParam("produto") String produtoVendido, @RequestParam("quantidade") int quantidadeLojistas) {
		
		EstoqueUbsApplication.estoque.getData().forEach(data -> {
			if(data.getProduct().equals(produtoVendido)) {
				estoqueProduto.getData().add(data);
			}
		});
		
		ObjectMapper m = new ObjectMapper();
		try {
			System.out.println(m.writeValueAsString(EstoqueUbsApplication.estoque));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		List<Product> listProduct = new ArrayList<Product>();
		
		int modQuantidade = estoqueProduto.sumOfQuantity() % quantidadeLojistas;
		
		for(int i=0; i<quantidadeLojistas; i++) {

			Product productFinal = new Product();
			Set<Data> setData = new HashSet<Data>();
			productFinal.setData(setData);
			productFinal.setId("p"+(i+1));
	
			for(Data d : estoqueProduto.getData()) {
				
				Integer quantityLojista = d.getQuantity() / quantidadeLojistas;
				int modQuantityLojista = d.getQuantity() % quantidadeLojistas;
				
				if(modQuantityLojista == 0) {
					Data data = new Data(d.getProduct(), quantityLojista, d.getPrice());
					productFinal.getData().add(data);
				} else {
	
					// Auxiliares
					Product productAux1 = copyProduct(productFinal);
					Data data1 = this.getDataAuxiliar(d.getProduct(), quantityLojista, d.getPrice());
					productAux1.getData().add(data1);
					
					Product productAux2 =  copyProduct(productFinal);
					Data data2 = this.getDataAuxiliar(d.getProduct(), quantityLojista + 1, d.getPrice());
					productAux2.getData().add(data2);
					
					boolean existe1 = false;
					existe1 = existeData(listProduct, data1, existe1);
					
					boolean existe2 = false;
					existe2 = existeData(listProduct, data2, existe2);
					
					if(!existe1 && !existe2) {
						productFinal.getData().add(data1);
					} else if(!existe1 && existe2){
						productFinal.getData().add(data1);
					} else if(existe1 && !existe2) {
						productFinal.getData().add(data2);
					} else {
						productFinal.getData().add(data2);
					}
				}	
			}
			
			listProduct.add(productFinal);
			
		}
		
		System.out.println("RESULTADO 1");
		m = new ObjectMapper();
		try {
			System.out.println(m.writeValueAsString(listProduct));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		// Armazena os iguais
		HashMap<Product, Set<Data>> mFinal = new HashMap<Product, Set<Data>>();
		List<Product> listProductAux = new ArrayList<Product>();
		listProductAux.addAll(listProduct);
		
		for(int j=0; j<listProductAux.size(); j++) {
			Product p1 = listProductAux.get(j);

			if(j+1 < listProductAux.size()) {
				Product p2 = listProductAux.get(j+1);
				
				Set<Data> pIguais = p1.getData().stream()
						.filter(x -> p2.getData().stream()
							.filter(y -> p2.getData().contains(x))
							.anyMatch(y -> y.getPrice() == x.getPrice() && y.getQuantity() == x.getQuantity())).collect(Collectors.toSet());
				
				if(!mFinal.containsKey(p1)) {
					mFinal.put(p1, pIguais);
				}
				if(!mFinal.containsKey(p2)) {
					mFinal.put(p2, pIguais);
				}
			}

		}


		HashMap<Product, Product> mapMesmaQuantidade = new HashMap<Product, Product>();
		
		if(listProduct.size() > 1) {
			
			// Mesma quantidade
			if(modQuantidade == 0) {

				for(int j=0; j<listProduct.size(); j++) {
					Product p1 = listProduct.get(j);
					
					if(j+1 < listProduct.size()) {
						Product p2 = listProduct.get(j+1);
						
						Product pAux1 = new Product();
						pAux1.setId(p1.getId());
						Set<Data> pData1 = p1.getData().stream()
								.filter(x -> p2.getData().stream()
									.filter(y -> !p2.getData().contains(x))
								.anyMatch(y -> y.getPrice() == x.getPrice() && y.getQuantity() == y.getQuantity())).collect(Collectors.toSet());
						pAux1.setData(pData1);
												
						Product pAux2 = new Product();
						pAux2.setId(p2.getId());
						Set<Data> pData2 = p2.getData().stream()
								.filter(x -> p1.getData().stream()
									.filter(y -> !p1.getData().contains(x))
								.anyMatch(y -> y.getPrice() == x.getPrice() && y.getQuantity() == y.getQuantity())).collect(Collectors.toSet());
						pAux2.setData(pData2);
						
						
						if(!mapMesmaQuantidade.containsKey(p1)) {
							mapMesmaQuantidade.put(p1, pAux1);
						}
						if(!mapMesmaQuantidade.containsKey(p2)) {
							mapMesmaQuantidade.put(p2, pAux2);
						}
					}
				}
				
				listProduct = new ArrayList<Product>(mapMesmaQuantidade.values());
				
				for(int i=0; i<listProduct.size(); i++) {
					Product p1 = listProduct.get(i);
					
					if(i+1 < listProduct.size()) {
						Product p2 = listProduct.get(i+1);
						
						for(Data dd1 : p1.getData()) {
							for(Data dd2 : p2.getData()) {
			
								if(dd1.getPrice() == dd2.getPrice()) {
									if(p1.sumOfQuantity() > p2.sumOfQuantity()) {
										dd1.setQuantity(dd1.getQuantity()-1);
										dd2.setQuantity(dd2.getQuantity()+1);
									} else if(p1.sumOfQuantity() < p2.sumOfQuantity()) {
										dd1.setQuantity(dd1.getQuantity()+1);
										dd2.setQuantity(dd2.getQuantity()-1);
									}
								}
							}
						}
					}
				}
			}
		}
		
		
		System.out.println("RESULTADO 2");
		m = new ObjectMapper();
		try {
			System.out.println(m.writeValueAsString(listProduct));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
				
		
		// Avg
		for(int j=0; j<listProduct.size(); j++) {
			Product p1 = listProduct.get(j);
				
			if(j+1 <listProduct.size()) {
				Product p2 = listProduct.get(j+1);
				
				for(Data dd1 : p1.getData()) {
					for(Data dd2 : p2.getData()) {
	
						if(dd1.getPrice() == dd2.getPrice() && dd1.getQuantity() != dd2.getQuantity()) {

							Integer qtd1 = dd1.getQuantity();
							Integer qtd2 = dd2.getQuantity();
							
							BigDecimal t1 = estoqueProduto.avgPrice().subtract(p1.avgPrice());
							BigDecimal t2 = estoqueProduto.avgPrice().subtract(p2.avgPrice());
							
							if(dd1.getQuantity() > dd2.getQuantity()) {
								dd1.setQuantity(dd1.getQuantity() - 1);
								dd2.setQuantity(dd2.getQuantity() + 1);
							} else if(dd1.getQuantity() < dd2.getQuantity()) {
								dd1.setQuantity(dd1.getQuantity() + 1);
								dd2.setQuantity(dd2.getQuantity() - 1);
							}
							
							Integer qtd3 = dd1.getQuantity();
							Integer qtd4 = dd2.getQuantity();
							
							BigDecimal t3 = estoqueProduto.avgPrice().subtract(p1.avgPrice());
							BigDecimal t4 = estoqueProduto.avgPrice().subtract(p2.avgPrice());
							
							//1>2
							if(t1.subtract(t2).compareTo(t3.subtract(t4)) > 0) {
								dd1.setQuantity(qtd1);
								dd2.setQuantity(qtd2);
							} else if(t1.subtract(t2).compareTo(t3.subtract(t4)) < 0){
								dd1.setQuantity(qtd3);
								dd2.setQuantity(qtd4);
							}
						}
					}
				}
			}
		}
		
		Iterator<Entry<Product, Set<Data>>> entryIterator = mFinal.entrySet().iterator();
		Entry<Product, Set<Data>> entry;
		List<Product> listProductFinal = new ArrayList<Product>();
		
		while(entryIterator.hasNext()) {
		    entry = entryIterator.next();
		    for(Product p : listProduct) {
	        	if(p.getId().equals(entry.getKey().getId())) {
			    	Product productFinal = copyProduct(p);
	        		productFinal.getData().addAll(entry.getValue());
	        		listProductFinal.add(productFinal);
	        	}
		    }
		}
		
		System.out.println("RESULTADO FINAL");
		m = new ObjectMapper();
		try {
			System.out.println(m.writeValueAsString(listProductFinal));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return listProductFinal;
	}

	private boolean existeData(List<Product> listProduct, Data data, boolean existe) {
		int k = 0;
		while(!existe && k < listProduct.size()) {
			if(listProduct.get(k).getData().contains(data)) {
				System.out.println("EXISTE");
				existe = true;
			}
			k++;
		}
		
		return existe;
	}
	
	private Product copyProduct(Product productFinal) {
		Product productAux1 = new Product();
		productAux1.setData(new HashSet<Data>());
		productAux1.getData().addAll(productFinal.getData());

		return productAux1;
	}
	
	private Data getDataAuxiliar(String strProduct, Integer quantity, BigDecimal price) {
		Data data = new Data(strProduct, quantity, price);
		
		return data;
	}
	
}
