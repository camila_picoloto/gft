package com.ubs.estoque.estoqueubs.util;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParsePosition;
import java.time.format.DateTimeParseException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class LocalCurrencyDeserializer extends JsonDeserializer<BigDecimal> {

	@Override
	public BigDecimal deserialize(JsonParser p, DeserializationContext ctx)
			throws IOException {
		
		try {
			
			JsonNode node = p.getCodec().readTree(p);
			String s = node.asText();
			
			if(s != null) { 
				//return new BigDecimal((s.substring(1,s.length()))).setScale(5, RoundingMode.FLOOR);
				
				DecimalFormatSymbols symbols = new DecimalFormatSymbols();
				symbols.setDecimalSeparator('.');

				DecimalFormat df = new DecimalFormat();
				df.setDecimalFormatSymbols(symbols);
				df.setParseBigDecimal(true);
				 
				ParsePosition pos = new ParsePosition(1);
				double d1 = df.parse(s, pos).doubleValue();
				BigDecimal price = new BigDecimal(d1).setScale(2, RoundingMode.HALF_EVEN);
					
				System.out.println(price);
				
				return price;

			} else {
				return new BigDecimal(0);
			}
			
		} catch (DateTimeParseException e) {
			System.err.println(e);
			return new BigDecimal(0);
		}
	}

}
